function changeName(){
    layer.open({
        type: 1,
        title:"设置昵称",
        area: ['600px', '250px'],
        shadeClose: true, //点击遮罩关闭
        content: '\<\div class="form-box">\n' +
        '        <div class="form-label"><i>*</i><span>设置昵称:</span><input type="text" placeholder="请输入你的昵称" id="name" value="" onchange="check()"/></div>\n' +
        '        <div class="ant-form-explain" id="test">请填写您的昵称，最多7个字符</div>\n'+
        '        <input type="button" value="确定" class="suy" onclick="setname()" />\n' +
        '    \<\/div>'+
        '\<\script>document.getElementById("name").value = $.cookie("nick_name");\<\/script>'
    });
}
//检查输入的昵称的长度，最多7个字符
function check() {
    var abc = $("#name").val();
    var len = abc.length;
    if (len > 7 || len == 0) {
        $("#test").css("display", "block");
        $(".suy").removeAttr("onclick");
    } else {
        $(".suy").attr("onclick", "setname()");
    }
}
//设置昵称
function setname() {
    $(function () {
        layer.closeAll();
            //修改成功后再弹出一层提示修改成功的框
            layer.open({
                type: 1,
                title: "修改成功",
                area: ['400px', '150px'],
                shadeClose: true, //点击遮罩关闭
                content: '\<\div class="box6"><div class="box6left">\n' +
                    '    <div class="box6lftcen">\n' +
                    '      <img src="/public/all/image/cel/quer.png">\n' +
                    '    </div>\n' +
                    '    </div>\n' +
                    '    <div class="box6rig">\n' +
                    '        <div class="box6rigcen">\n' +
                    '            <p>昵称已经修改</p >\n' +
                    '        </div>\n' +
                    '\n' +
                    '    </div> \<\/div>'
            });
            setTimeout(function () {
                layer.closeAll();
            }, 1000); //延迟1秒后关闭 
        // $.ajax({
        //     url: "/API/index/nickname",
        //     data: {
        //         nickname: $("#name").val(),
        //         username: $.cookie("username")
        //     },
        //     dataType: "json",
        //     method: "post",
        //     async: true,
        //     success: function (data) {
                
        //         var dataObj = JSON.parse(data);
        //         if (dataObj.status == 1) {
        //             //用户名存cookie
                    
        //             var nick_name = $.cookie("nick_name");
        //             $(".siot").html(nick_name);
        //             layer.closeAll();
        //             //修改成功后再弹出一层提示修改成功的框
        //             layer.open({
        //                 type: 1,
        //                 title: "修改成功",
        //                 area: ['400px', '150px'],
        //                 shadeClose: true, //点击遮罩关闭
        //                 content: '\<\div class="box6"><div class="box6left">\n' +
        //                     '    <div class="box6lftcen">\n' +
        //                     '      <img src="/public/all/image/cel/quer.png">\n' +
        //                     '    </div>\n' +
        //                     '    </div>\n' +
        //                     '    <div class="box6rig">\n' +
        //                     '        <div class="box6rigcen">\n' +
        //                     '            <p>昵称已经修改</p >\n' +
        //                     '        </div>\n' +
        //                     '\n' +
        //                     '    </div> \<\/div>'
        //             });
        //             setTimeout(function () {
        //                 layer.closeAll();
        //             }, 1000); //延迟1秒后关闭 

        //         } else {
        //             alert("修改失败");
        //         }
        //     }
        // })
    })
}